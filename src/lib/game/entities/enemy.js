ig.module(
    'game.entities.enemy'
)
.requires(
    'impact.entity'
)
.defines(function() {

    EntityEnemy = ig.Entity.extend({

        size: { x: 12, y: 16 },
        offset: { x: 2, y: 2 },
        health: 200,
        collides: ig.Entity.COLLIDES.PASSIVE,
        type: ig.Entity.TYPE.B,
        checkAgainst: ig.Entity.TYPE.A,

        animSheet: new ig.AnimationSheet('media/orc.png', 16, 18),

        init: function (x, y, settings) {

            this.parent(x, y, settings);

            this.addAnim('idleU', 0.6, [1,2,1,0]);
            this.addAnim('idleD', 0.6, [19,20,19,18]);
            this.addAnim('idleL', 0.6, [10,11,10,9]);
            this.addAnim('idleR', 0.6, [28,29,28,27]);

            this.addAnim('walkU', 0.15, [1,2,1,0]);
            this.addAnim('walkD', 0.15, [19,20,19,18]);
            this.addAnim('walkL', 0.15, [28,29,28,27]);
            this.addAnim('walkR', 0.15, [10,11,10,9]);
        },

        update: function() {

            this.parent();
            this.currentAnim = this.anims.idleD
        }
    });
});
